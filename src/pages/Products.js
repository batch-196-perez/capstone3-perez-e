

import {useState, useEffect} from 'react';
import productData from '../data/productData';
import ProductCard from '../components/ProductCard';

export default function Product(){

	const [products, setProducts] = useState([]);

	useEffect(() => {
		fetch('http://localhost:4000/products')
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setProducts(data.map(product => {
				return(
					<ProductCard key={product._id} productProp = {product}/>
				)
			}))
		})
	})

	
		
		
	

	return (
		//<ProductCard/>
		<>
		<h1> Available Courses: </h1>
		{products}
		</>
	)
}