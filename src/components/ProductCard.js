import {useState} from 'react';
import {Card, Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function Product({productProp}){

	const {name, description, price, _id} = productProp;

	// const [count, setCount] = useState(0);
	// const [seatCount, seatSetCount] = useState(10);
	// console.log(useState(0));

	// function enroll(){
	// 	setCount(count + 1);
	// 	seatSetCount(seatCount -1)
	// 	if (count === 10 && seatCount === 0){
	// 		setCount (count);
	// 		seatSetCount(seatCount)
	// 		alert("No more seats available. Check back later!");
	// 	}
	// 	//console.log(`Enrollesss: ${count}`);
	// }

	return (
		<Card className ="cardCourse p-3 mb-5">
			<Card.Body>
				<Card.Title className="fw-bold">{name}</Card.Title>
				<Card.Subtitle>Description: </Card.Subtitle>
				<Card.Text>{description}</Card.Text>
				<Card.Subtitle>Course Price:</Card.Subtitle>
				<Card.Text>{price}</Card.Text>
				<Link className="btn btn-primary" to={`/productView/${_id}`}>View Details</Link>
				
			</Card.Body>
		</Card>
	)
}